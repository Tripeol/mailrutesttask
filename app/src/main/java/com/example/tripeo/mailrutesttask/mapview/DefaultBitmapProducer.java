package com.example.tripeo.mailrutesttask.mapview;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;

import com.example.tripeo.mailrutesttask.mapview.cache.IMapViewCacheManager;
import com.example.tripeo.mailrutesttask.network.INetworkManager;
import com.example.tripeo.mailrutesttask.rx_java.SimpleObserver;

import io.reactivex.annotations.NonNull;

/**
 * @author Trikhin P O
 * @date 09.08.2017
 *
 * Класс реализующий {@link IBitmapProducer}, и поставляющий {@link Bitmap} для {@link MailRuMapView}.
 *
 * Данная реализация имеемт возможность загружать {@link Bitmap} либо из  сети,
 * либо из InternalStorage.
 */

public class DefaultBitmapProducer implements IBitmapProducer {

    private IMapViewCacheManager mMapViewCacheManager;
    private INetworkManager mNetworkManager;

    private SparseArray<SparseArray<Boolean>> mIsLoadingMap;

    public DefaultBitmapProducer(IMapViewCacheManager mapViewCacheManager, INetworkManager networkManager) {
        mMapViewCacheManager = mapViewCacheManager;
        mNetworkManager = networkManager;
        mIsLoadingMap = new SparseArray<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Bitmap obtainBitmap(final int x, final int y, final @Nullable IOnBitmapLoadedListener onBitmapLoadedListener) {

            if (mMapViewCacheManager.containsInCache(x, y)) {
                loadBitmapFromInternalStorage(x, y, onBitmapLoadedListener);
                return null;
            } else {
                loadBitmapFromWeb(x, y, onBitmapLoadedListener);
                return null;
            }

    }

    /**
     * Получение Битмап из внутреннего хранилища
     */
    private void loadBitmapFromInternalStorage(final int x, final int y, final IOnBitmapLoadedListener onBitmapLoadedListener){
        if (!isTileLoading(x, y)) {
            setTileLoading(x, y, true);
        }
        mMapViewCacheManager.getFromCache(x, y).subscribe(new SimpleObserver<Bitmap>() {
            @Override
            public void onNext(@NonNull Bitmap bitmap) {
                if (onBitmapLoadedListener != null) {
                    onBitmapLoadedListener.onBitmapLoaded(x, y, bitmap);
                }
                setTileLoading(x, y, false);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                setTileLoading(x, y, false);
            }
        });
    }

    /**
     * Получение Битмап из сети
     */
    private void loadBitmapFromWeb(final int x, final  int y, final IOnBitmapLoadedListener onBitmapLoadedListener){
        if (!isTileLoading(x, y)) {
            setTileLoading(x, y, true);
            mNetworkManager.getTile(x, y).subscribe(new SimpleObserver<Bitmap>() {
                @Override
                public void onNext(@NonNull Bitmap bitmap) {
                    mMapViewCacheManager.addToCache(x, y, bitmap);
                    if (onBitmapLoadedListener != null) {
                        onBitmapLoadedListener.onBitmapLoaded(x, y, bitmap);
                    }
                    setTileLoading(x, y, false);
                }

                @Override
                public void onError(@NonNull Throwable throwable) {
                    Log.d("Error", "onBitmapLoad Error");
                    setTileLoading(x, y, false);
                }
            });

        }
    }


    private void setTileLoading(int x, int y, boolean value) {
        if (mIsLoadingMap.get(x) != null) {
            mIsLoadingMap.get(x).put(y, value);
        } else {
            SparseArray<Boolean> newXArray = new SparseArray<>();
            newXArray.put(y, value);
            mIsLoadingMap.put(x, newXArray);
        }
    }

    private boolean isTileLoading(int x, int y) {
        if (mIsLoadingMap.get(x) == null) {
            return false;
        } else if (mIsLoadingMap.get(x).get(y) == null) {
            return false;
        } else {
            return mIsLoadingMap.get(x).get(y);
        }
    }
}
