package com.example.tripeo.mailrutesttask.network;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.tripeo.mailrutesttask.network.api.IMapApiMapper;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Trikhin P O
 * @date 04.08.2017
 * <p>
 * Дефолтная реализация {@link INetworkManager}
 */

public class DefaultNetworkManager implements INetworkManager {

    private final IMapApiMapper mMapApiMapper;

    public DefaultNetworkManager(IMapApiMapper mapApiMapper) {
        mMapApiMapper = mapApiMapper;
    }

    @Override
    public void testMethod() {
        Log.d("delme", "this is test metod");
    }

    @Override
    public Observable<Bitmap> getTile(final int x, final int y) {
        return Observable.fromCallable(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                return mMapApiMapper.getTile(x, y);
            }
        }).
                subscribeOn(Schedulers.newThread()).
                observeOn(AndroidSchedulers.mainThread());
    }
}
