package com.example.tripeo.mailrutesttask;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.tripeo.mailrutesttask.diaogs.InsertCentralTileDialog;
import com.example.tripeo.mailrutesttask.mapview.DefaultBitmapProducer;
import com.example.tripeo.mailrutesttask.mapview.MailRuMapView;
import com.example.tripeo.mailrutesttask.mapview.cache.IMapViewCacheManager;
import com.example.tripeo.mailrutesttask.network.INetworkManager;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    INetworkManager mNetworkManager;
    @Inject
    IMapViewCacheManager mCacheManager;

    private MailRuMapView mMapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MailRuTestApplication)getApplication()).getCoreComponent().inject(this);

        setContentView(R.layout.activity_main);
        mMapView = (MailRuMapView) findViewById(R.id.map_view);

        mMapView.setBitmapProducer(new DefaultBitmapProducer(mCacheManager, mNetworkManager));
        mMapView.setCentralTile(33198, 22539);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.insert_tile:
                InsertCentralTileDialog.getInstance(mMapView).show(getFragmentManager(), InsertCentralTileDialog.TAG);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
