package com.example.tripeo.mailrutesttask.mapview;

import android.graphics.Bitmap;

/**
 * @author Trikhin P O
 * @date 09.08.2017
 *
 * Слушатель того что {@link android.graphics.Bitmap} загрузился
 */

public interface IOnBitmapLoadedListener {

    void onBitmapLoaded(int x, int y, Bitmap bitmap);

}
