package com.example.tripeo.mailrutesttask;

import android.app.Application;

import com.example.tripeo.mailrutesttask.dagger.components.CoreComponent;
import com.example.tripeo.mailrutesttask.dagger.components.DaggerCoreComponent;
import com.example.tripeo.mailrutesttask.dagger.modules.AppModule;

/**
 * @author Trikhin P O
 * @date 04.08.2017
 */

public class MailRuTestApplication extends Application {

    private CoreComponent sCoreComponent;

    public CoreComponent getCoreComponent(){
        return sCoreComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sCoreComponent = buildCoreComponent();
    }

    private CoreComponent buildCoreComponent(){
        return DaggerCoreComponent.builder().appModule(new AppModule(this)).build();
    }
}
