package com.example.tripeo.mailrutesttask.rx_java;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Created by Петр on 07.08.2017.
 */

public abstract class SimpleObserver<T>  implements Observer<T> {
    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    @Override
    public void onComplete() {

    }
}
