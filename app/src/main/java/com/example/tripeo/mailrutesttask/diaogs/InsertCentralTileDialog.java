package com.example.tripeo.mailrutesttask.diaogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.tripeo.mailrutesttask.R;
import com.example.tripeo.mailrutesttask.mapview.MailRuMapView;

/**
 * @author Trikhin P O
 * @date 08.07.2017
 *
 * Диалог позволяющий установить центральный тайл карты
 */

public class InsertCentralTileDialog extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "InsertCentralTileDialog";

    private static final String MAP_VIEW_KEY = "mapView";

    private MailRuMapView mMapView;

    private Button mNextButton;

    private EditText mEditTextX;
    private EditText mEditTextY;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(MAP_VIEW_KEY)) {
            mMapView = (MailRuMapView) getArguments().getSerializable(MAP_VIEW_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View dialogView = inflater.inflate(R.layout.insert_central_fragment_dialog, container, false);
        mNextButton = (Button) dialogView.findViewById(R.id.next);
        mNextButton.setOnClickListener(this);

        mEditTextX = (EditText) dialogView.findViewById(R.id.x_xord_edit_text);
        mEditTextY = (EditText) dialogView.findViewById(R.id.y_xord_edit_text);

        return dialogView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next:
                int x = Integer.parseInt(mEditTextX.getText().toString());
                int y = Integer.parseInt(mEditTextY.getText().toString());
                mMapView.setCentralTile(x,y);
                dismiss();
                break;
        }
    }


    public static InsertCentralTileDialog getInstance(MailRuMapView mailRuMapView) {
        Bundle args = new Bundle();
        args.putSerializable(MAP_VIEW_KEY, mailRuMapView);
        InsertCentralTileDialog fragment = new InsertCentralTileDialog();
        fragment.setArguments(args);
        return fragment;
    }

}
