package com.example.tripeo.mailrutesttask.dagger.modules;

import com.example.tripeo.mailrutesttask.network.api.DefaultMapApiMapper;
import com.example.tripeo.mailrutesttask.network.DefaultNetworkManager;
import com.example.tripeo.mailrutesttask.network.api.IMapApiMapper;
import com.example.tripeo.mailrutesttask.network.INetworkManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Trikhin P O
 * @date 04.08.2017
 *
 * Модуль инжектящий классы взаимодействия с сетью
 */

@Module
@Singleton
public class NetworkModule {

    @Provides
    @Singleton
    INetworkManager provideINetworkManager(){
        IMapApiMapper mapApiMapper = new DefaultMapApiMapper();
        return new DefaultNetworkManager(mapApiMapper);
    }

}
