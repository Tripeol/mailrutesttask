package com.example.tripeo.mailrutesttask.dagger.components;

import com.example.tripeo.mailrutesttask.MainActivity;
import com.example.tripeo.mailrutesttask.dagger.modules.AppModule;
import com.example.tripeo.mailrutesttask.dagger.modules.CacheModule;
import com.example.tripeo.mailrutesttask.dagger.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Trikhin P O
 * @date 04.08.2017
 */

@Component(modules = {AppModule.class, NetworkModule.class, CacheModule.class})
@Singleton
public interface CoreComponent {

    void inject(MainActivity mainActivity);

}
