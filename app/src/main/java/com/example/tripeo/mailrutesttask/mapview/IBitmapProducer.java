package com.example.tripeo.mailrutesttask.mapview;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

/**
 * @author Trikhin P O
 * @date 09.08.2017
 *
 * интерфейс поставляющий тайлы для {@link MailRuMapView}
 */

public interface IBitmapProducer {

    /**
     * Получить  {@link Bitmap}
     * @param x
     * @param y
     * @param onBitmapLoadedListener - слушатель на случай асинхронного получения {@link Bitmap}
     * @return
     */
    @Nullable
    Bitmap obtainBitmap(int x, int y, @Nullable IOnBitmapLoadedListener onBitmapLoadedListener);

}
