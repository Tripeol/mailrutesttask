package com.example.tripeo.mailrutesttask.mapview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;

import com.example.tripeo.mailrutesttask.R;

import java.io.Serializable;
import java.lang.ref.SoftReference;

/**
 * @author Trikhin P O
 * @date 09.08.2017
 *
 * Класс наследующийся от {@link View} использующийся для отображения карты.
 *
 * Для работы необходимо предоставить реализацию интерфейса {@link IBitmapProducer}, которая будет
 * предоставлять {@link Bitmap} для отрисовки.
 *
 * Неочевидная работа с кешем: все битмапы хранятся в коллекции {@link MailRuMapView#mSoftCacheMap}.
 * Но это коллекция из слабых ссылок - время от времени сборщик мусора будет ее очищать
 * При этом есть вспомогательная коллекция {@link MailRuMapView#mStrongCacheMap} - она содержит сильные ссылки
 * только на те {@link Bitmap} из первой коллекци которые отображаются на экране.
 * Таким образом сборщик мусора никогда не очищает те {@link Bitmap} которые находятся на экране
 *
 * Также есть важный параметр {@link MailRuMapView#mPrefetchBorder}. Его можно выставить через разметку либо же
 * будет использовано значение по умолчанию {@link MailRuMapView#DEFAULT_PREFETCH_BORDER}. Этот параметр отвечает за размер слоя тайлов,
 * которые находятся вокруг видимой части тайлов и которые подгружаются заранее (до того как станут видимыми).
 * Например:
 * если выставить значение в 0 - то загрузка каждого тайла начнется как только он станет видимым.
 * если выставить значение в 2 - то загрузка каждого тайла начнется заранее, в случае если он будет на расстоянии 2 от любого видимого
 * в данный момент тайла
 * Увеличение параметра ведет к снижению производительности, но при этом улучшает пользовательский опыт.
 *
 * Можно установить центральный тайл на который переместися экран, метдом {@link MailRuMapView#setCentralTile(int, int)}
 */

public class MailRuMapView extends View implements IOnBitmapLoadedListener, Serializable {

    private static final int DEFAULT_TITLE_WIDTH = 256;
    private static final int DEFAULT_TITLE_HEIGHT = 256;
    private static final int DEFAULT_PREFETCH_BORDER = 1;

    private float mAnchorX = 0;
    private float mAnchorY = 0;

    private int mTitleWidth;
    private int mTitleHeight;

    private Pair<Integer, Integer> centralTile;

    private boolean wasCentered = false;

    private IBitmapProducer mBitmapProducer;

    /**
     * В этой коллекции хранятся {@link Bitmap} и спользуемые для рисования карты
     * Важно что здесь только слабые ссылки
     */
    private SparseArray<SparseArray<SoftReference<Bitmap>>> mSoftCacheMap;

    /**
     * Эта коллекция вспомогательная. В ней хранятся ссылки на теже самые {@link Bitmap},
     * которые находятся в {@link MailRuMapView#mSoftCacheMap} но только те что в данный момент отрисовываются во вьюхе.
     */
    private SparseArray<SparseArray<Bitmap>> mStrongCacheMap;

    /**
     * см документацию к классу {@link MailRuMapView}
     */
    private int mPrefetchBorder;

    private float mLastTouchX;
    private float mLastTouchY;

    public MailRuMapView(Context context) {
        this(context, null);
    }

    public MailRuMapView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MailRuMapView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.MailRuMapView,
                0, 0);

        try {
            mTitleWidth = (int) a.getDimension(R.styleable.MailRuMapView_tile_size_x, DEFAULT_TITLE_WIDTH);
            mTitleHeight = (int)a.getDimension(R.styleable.MailRuMapView_tile_size_y, DEFAULT_TITLE_HEIGHT);
            mPrefetchBorder = a.getInteger(R.styleable.MailRuMapView_prefetch_border, DEFAULT_PREFETCH_BORDER);
            if (mPrefetchBorder <0 ){
                throw  new IllegalArgumentException("PrefetchBorder must be positive number");
            }
        } finally {
            a.recycle();
        }

        mSoftCacheMap = new SparseArray<>();
        mStrongCacheMap = new SparseArray<>();
        centralTile = new Pair<>(0, 0);
        mAnchorY = 0;
        mAnchorX = 0;
    }

    public void setBitmapProducer(IBitmapProducer bitmapProducer) {
        mBitmapProducer = bitmapProducer;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!wasCentered) {
            mAnchorX = (getWidth() - mTitleWidth) / 2;
            mAnchorY = (getHeight() - mTitleHeight) / 2;
            wasCentered = true;
        }
        if (mBitmapProducer == null){
            return;
        }

        drawTiles(canvas);
    }

    @Override
    public void onBitmapLoaded(int x, int y, Bitmap bitmap) {
        addBitmapToMemory(bitmap, x, y);
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastTouchX = event.getX();
                mLastTouchY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
                float dx = event.getX() - mLastTouchX;
                float dy = event.getY() - mLastTouchY;
                mAnchorX = mAnchorX + dx;
                mAnchorY = mAnchorY + dy;

                mLastTouchX = event.getX();
                mLastTouchY = event.getY();
                reorganizeRamCache();
                invalidate();
                break;
        }
        return true;
    }

    /**
     * Метод позволяет установить центральный тайл на который переместится экран
     */
    public void setCentralTile(int x, int y) {
        centralTile = new Pair<>(x, y);
        wasCentered = false;
        invalidate();
    }

    private void drawTiles(Canvas canvas) {
        Pair<Integer, Integer> topLeft = calculateTopLeftTile();
        Pair<Integer, Integer> bottomRight = calculateBottomRightTile();

        Bitmap bitmap;

        for (int i = topLeft.second; i <= bottomRight.second; i++) {
            for (int j = topLeft.first; j <= bottomRight.first; j++) {
                if (isBitmapInMemory(j, i)) {
                    bitmap = mSoftCacheMap.get(j).get(i).get();
                    canvas.drawBitmap(bitmap,
                            mAnchorX - (centralTile.first - j) * mTitleWidth,
                            mAnchorY - (centralTile.second - i) * mTitleHeight,
                            null);
                } else {
                    mBitmapProducer.obtainBitmap(j, i, this);
                }

            }
        }
    }

    private Pair<Integer, Integer> calculateTopLeftTile() {
        int x_diff_in_tiles = 0;
        int y_diff_in_tiles = 0;

        if (mAnchorX > 0) {
            x_diff_in_tiles = ((int) (mAnchorX / mTitleWidth)) + 1;
        } else {
            x_diff_in_tiles = (int) (mAnchorX / mTitleWidth);
        }

        if (mAnchorY > 0) {
            y_diff_in_tiles = ((int) (mAnchorY / mTitleHeight)) + 1;
        } else {
            y_diff_in_tiles = (int) (mAnchorY / mTitleHeight);
        }

        return new Pair<>(centralTile.first - x_diff_in_tiles - DEFAULT_PREFETCH_BORDER,
                centralTile.second - y_diff_in_tiles - DEFAULT_PREFETCH_BORDER);
    }

    private Pair<Integer, Integer> calculateBottomRightTile() {
        int x_diff_in_tiles = 0;
        int y_diff_in_tiles = 0;

        if ((getWidth() - mAnchorX) > 0) {
            x_diff_in_tiles = (int) ((getWidth() - mAnchorX) / mTitleWidth);
        } else {
            x_diff_in_tiles = (int) ((getWidth() - mAnchorX) / mTitleWidth) - 1;
        }

        if ((getHeight() - mAnchorY) > 0) {
            y_diff_in_tiles = (int) ((getHeight() - mAnchorY) / mTitleHeight);
        } else {
            y_diff_in_tiles = (int) ((getHeight() - mAnchorY) / mTitleHeight) - 1;
        }

        return new Pair<>(centralTile.first + x_diff_in_tiles + DEFAULT_PREFETCH_BORDER,
                centralTile.second + y_diff_in_tiles + DEFAULT_PREFETCH_BORDER);
    }

    private boolean isBitmapInMemory(int x, int y) {
        return mSoftCacheMap.get(x) != null &&
                mSoftCacheMap.get(x).get(y) != null &&
                mSoftCacheMap.get(x).get(y).get() != null;

    }

    private void addBitmapToMemory(Bitmap bitmap, int x, int y) {
        if (mSoftCacheMap.get(x) == null) {
            mSoftCacheMap.put(x, new SparseArray<SoftReference<Bitmap>>());
        }
        mSoftCacheMap.get(x).put(y, new SoftReference<>(bitmap));
    }

    /**
     * При пролистывании необходимо очищать ссылки на {@link Bitmap}, которые вышли за пределы {@link MailRuMapView}.
     * Если этого не делать рано или поздно упадем с OutOfMemory
     */
    private void reorganizeRamCache() {
        Pair<Integer, Integer> topLeftTile = calculateTopLeftTile();
        Pair<Integer, Integer> bottomRightTile = calculateBottomRightTile();

        for (int row = topLeftTile.first; row <= bottomRightTile.first; row++) {
            if (mStrongCacheMap.get(row - topLeftTile.first) == null) {
                mStrongCacheMap.put(row - topLeftTile.first, new SparseArray<Bitmap>());
            } else {
                mStrongCacheMap.get(row - topLeftTile.first).clear();
            }
            for (int collumn = topLeftTile.second; collumn <= bottomRightTile.second; collumn++) {
                if (isBitmapInMemory(row, collumn)) {
                    mStrongCacheMap.get(row - topLeftTile.first).put(collumn - topLeftTile.second, mSoftCacheMap.get(row).get(collumn).get());
                }
            }
        }
    }

}
