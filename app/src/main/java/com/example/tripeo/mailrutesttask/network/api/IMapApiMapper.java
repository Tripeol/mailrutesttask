package com.example.tripeo.mailrutesttask.network.api;

import android.graphics.Bitmap;

/**
 * @author Trikhin P O
 * @date 07.08.2017
 *
 * Интерфейс вызовов в сеть
 */

public interface IMapApiMapper {

    Bitmap getTile(int x, int y);

}
