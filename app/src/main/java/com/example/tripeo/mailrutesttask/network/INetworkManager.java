package com.example.tripeo.mailrutesttask.network;

import android.graphics.Bitmap;

import io.reactivex.Observable;

/**
 * @author Trikhin P O
 * @date 04.08.2017
 *
 * Интерфейс менеджера для взаимодействия с сетью
 */

public interface INetworkManager {

    void testMethod();

    Observable<Bitmap> getTile(int x, int y);
}
