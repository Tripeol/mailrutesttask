package com.example.tripeo.mailrutesttask.mapview.cache;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Trikhin P O
 * @date 09.08.2017
 *
 * дефолтная реализация {@link IMapViewCacheManager}
 *
 * Данный класс реализует работу с InternalStorage.
 *
 * Тут есть две константы которые задаются на этапе компиляции (не стал выносить их в метод интерфейса)
 *
 * {@link DefaultMapViewCacheManager#MAX_FILES_IN_INTERNAL_MEMORY}
 * указывает максимальное количество тайлов которое будет хранится в InternalMemory. При превышении этого
 * количества - часть тайлов будет удалена
 *
 * {@link DefaultMapViewCacheManager#FILES_TO_REMOVE}
 * указывает сколько файлов будет удалено за один раз при превышении лимита в количество файлов. Первыми будут
 * удаляться файлы которые наиболее старые
 */

public class DefaultMapViewCacheManager implements IMapViewCacheManager {

    private static final String TILE_FILE_NAME_TEMPLATE = "MailRuTile_%1$d_%2$d";
    private static final String TILE_DIRECTORY_NAME = "tiles";

    private static final int MAX_FILES_IN_INTERNAL_MEMORY = 10000;
    private static final int FILES_TO_REMOVE = 500;

    private Context mContext;

    public DefaultMapViewCacheManager(Context context) {
        mContext = context;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsInCache(int x, int y) {
        ContextWrapper cw = new ContextWrapper(mContext.getApplicationContext());
        File directory = cw.getDir(TILE_DIRECTORY_NAME, Context.MODE_PRIVATE);
        String fileName = String.format(Locale.ENGLISH, TILE_FILE_NAME_TEMPLATE, x, y);
        File filePath = new File(directory, fileName);
        if (filePath.exists()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Observable<Bitmap> getFromCache(final int x, final int y) {
        return Observable.fromCallable(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                return loadBitmapFromInternalStorage(x, y);
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addToCache(int x, int y, Bitmap bitmap) {
        tryToDeleteOldFiles();
        saveBitmapToInternalStorage(x, y, bitmap);
    }

    private void saveBitmapToInternalStorage(int x, int y, Bitmap bitmap) {
        ContextWrapper cw = new ContextWrapper(mContext.getApplicationContext());
        File directory = cw.getDir(TILE_DIRECTORY_NAME, Context.MODE_PRIVATE);
        String fileName = getFileName(x, y);
        File filePath = new File(directory, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap loadBitmapFromInternalStorage(int x, int y) {
        try {
            ContextWrapper cw = new ContextWrapper(mContext.getApplicationContext());
            File directory = cw.getDir(TILE_DIRECTORY_NAME, Context.MODE_PRIVATE);
            File f = new File(directory, getFileName(x, y));
            return BitmapFactory.decodeStream(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getFileName(int x, int y) {
        return String.format(Locale.ENGLISH, TILE_FILE_NAME_TEMPLATE, x, y);
    }

    private synchronized void tryToDeleteOldFiles(){
        ContextWrapper cw = new ContextWrapper(mContext.getApplicationContext());
        File directory = cw.getDir(TILE_DIRECTORY_NAME, Context.MODE_PRIVATE);
        List<File> files = Arrays.asList(directory.listFiles());
        if (files.size() > MAX_FILES_IN_INTERNAL_MEMORY){
            Collections.sort(files, new Comparator<File>() {
                public int compare(File o1, File o2) {
                    if (o1.lastModified() > o2.lastModified()){
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });
            for (int i = files.size()-1; i > MAX_FILES_IN_INTERNAL_MEMORY - FILES_TO_REMOVE; i --){
                files.get(i).delete();
            }
        }

    }
}
