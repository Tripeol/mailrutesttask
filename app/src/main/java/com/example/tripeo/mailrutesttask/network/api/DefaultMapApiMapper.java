package com.example.tripeo.mailrutesttask.network.api;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

/**
 * @author Trikhin P O
 * @date 08.07.2017
 * <p>
 * Дефолтная реализация {@link IMapApiMapper}
 */

public class DefaultMapApiMapper implements IMapApiMapper {

    private static final String BASE_URL = "http://b.tile.opencyclemap.org/cycle/16/%d/%d.png";

    @Override
    public Bitmap getTile(int x, int y) {
        try {
            URL url = new URL(String.format(Locale.ENGLISH, BASE_URL, x, y));
            return BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
