package com.example.tripeo.mailrutesttask.dagger.modules;

import android.content.Context;

import com.example.tripeo.mailrutesttask.mapview.cache.DefaultMapViewCacheManager;
import com.example.tripeo.mailrutesttask.mapview.cache.IMapViewCacheManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Trikhin P O
 * @date 09.08.2017
 */

@Module
@Singleton
public class CacheModule {

    @Provides
    @Singleton
    IMapViewCacheManager provideMapViewCacheManager(Context context){
        return new DefaultMapViewCacheManager(context);
    }

}
