package com.example.tripeo.mailrutesttask.mapview.cache;

import android.graphics.Bitmap;

import io.reactivex.Observable;

/**
 * @author Trikhin P O
 * @date 09.08.2017
 *
 * Интерфейс закрывающий управление кешем для {@link Bitmap}
 */

public interface IMapViewCacheManager {

    /**
     * Проверяет находится ли {@link Bitmap} в кеше
     * по координатам x y
     *
     * @return true, если кеш не пустой
     */
    boolean containsInCache(int x, int y);

    /**
     * Получение {@link Bitmap} из кеша
     *
     * @return
     */
    Observable<Bitmap> getFromCache(int x, int y);

    /**
     * Добавление {@link Bitmap} в кеш
     */
    void addToCache(int x, int y, Bitmap bitmap);

}
